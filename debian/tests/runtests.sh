#!/bin/sh

set -e

rm -f /tmp/pwn

wsgidav --host=0.0.0.0 --port=8080 --auth=anonymous --root /tmp &
sleep 5

davtest -url http://127.0.0.1:8080
davtest -url http://127.0.0.1:8080 -uploadfile=/etc/passwd -uploadloc=pwn

pkill wsgidav

test -f /tmp/pwn
